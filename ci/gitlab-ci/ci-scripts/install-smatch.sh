#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024 Collabora, Helen Koike <helen.koike@collabora.com>

set -exo pipefail

pushd /
git clone --depth 1 https://repo.or.cz/smatch.git
pushd smatch
make
popd
popd
