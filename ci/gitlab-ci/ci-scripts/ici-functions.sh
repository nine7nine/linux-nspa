#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024 Collabora, Helen Koike <helen.koike@collabora.com>

set -exo pipefail

# internal CI bash functions

# convention:
# KCI_<VARIABLE_NAME> for variables defined by the user (outside of this script)
# ICI_<VARIABLE_NAME> for variables defined internally for usage between scripts
# CI_<VARIABLE_NAME> for variables defined by GitLab CI


ici_prepare_build() {
    BUILD_DIR="${1:-build}"

    echo ""
    echo "Architecture: $KCI_KERNEL_ARCH"
    echo "Defconfig: $KCI_DEFCONFIG"
    echo ""

    # Get the current directory if KCI_KERNEL_DIR is not set
    ICI_KERNEL_DIR="${KCI_KERNEL_DIR:-$(pwd)}"

    cd "$ICI_KERNEL_DIR" || { echo "Kernel directory not found"; exit 1; }

    # Clean up stale rebases that GitLab might not have removed when reusing a checkout dir
    rm -rf .git/rebase-apply

    if [[ "$KCI_KERNEL_ARCH" = "arm64" ]]; then
        GCC_ARCH="aarch64-linux-gnu"
    elif [[ "$KCI_KERNEL_ARCH" = "arm" ]]; then
        GCC_ARCH="arm-linux-gnueabihf"
    else
        GCC_ARCH="x86_64-linux-gnu"
    fi

    # do not set ARCH and CROSS_COMPILE if KCI_KERNEL_ARCH is not set, useful for local run
    if [ -n "$KCI_KERNEL_ARCH" ]; then
        export ARCH=${KCI_KERNEL_ARCH}
        export CROSS_COMPILE="${GCC_ARCH}-"
    fi

    mkdir -p "$BUILD_DIR"

    pushd "$BUILD_DIR" || { echo "Failed to create $BUILD_DIR directory"; exit 1; }

    # generate defconfig
    make -C "$ICI_KERNEL_DIR" O=$(pwd) $(basename ${KCI_DEFCONFIG-"defconfig"})

    # add extra configs from variable KCI_KCONFIGS_{ENABLE,DISABLE,MODULE}
    for opt in $KCI_KCONFIGS_ENABLE; do
        ../scripts/config --file .config --enable CONFIG_$opt
    done
    for opt in $KCI_KCONFIGS_DISABLE; do
        ../scripts/config --file .config --disable CONFIG_$opt
    done
    for opt in $KCI_KCONFIGS_MODULE; do
        ../scripts/config --file .config --module CONFIG_$opt
    done

    if [ -n "$KCI_KCONFIGS_DISABLE" ] || [ -n "$KCI_KCONFIGS_ENABLE" ] ||
       [ -n "$KCI_KCONFIGS_MODULE" ]; then
        # execude olddefconfig only if we changed the default config, otherwise,
        # let it raise warnings if any
        make -C "$ICI_KERNEL_DIR" O=$(pwd) olddefconfig
    fi

    popd
}

ici_get_patch_series_size()
{
    local CLONE_DEPTH
    CLONE_DEPTH=$(git rev-list --count HEAD)
    echo "The depth of the clone is $CLONE_DEPTH"

    # If this is in the context of a merge request, calculate the patch series
    # size comparing to the target branch
    if [ -n "$CI_MERGE_REQUEST_IID" ]; then
        git fetch origin "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" --depth $CLONE_DEPTH
        BASE_COMMIT="origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
        ICI_PATCH_SERIES_SIZE=$(git rev-list --count ${BASE_COMMIT}.."$CI_COMMIT_SHA")

    # if KCI_PATCH_SERIES_SIZE is set, use it
    elif [ -n "$KCI_PATCH_SERIES_SIZE" ]; then
        ICI_PATCH_SERIES_SIZE="$KCI_PATCH_SERIES_SIZE"
    else
        ICI_PATCH_SERIES_SIZE=1
        echo "WARNING: unable to detect the patch series size, using the default value of 1."
        # shellcheck disable=SC2034
        ICI_UNABLE_TO_DETECT_PATCH_SERIES_SIZE=true
    fi

    # Check if the clone depth is smaller than or equal to KCI_PATCH_SERIES_SIZE,
    # otherwise the checkpatch.pl hangs
    if [ "$ICI_PATCH_SERIES_SIZE" -ge "$CLONE_DEPTH" ]; then
        echo -n "ERROR: the depth of the clone is $CLONE_DEPTH, smaller than or equal to the patch"
        echo " series size. Update your GitLab configuration to increase the size of the clone."
        return 1
    fi
}
