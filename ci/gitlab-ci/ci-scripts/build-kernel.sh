#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024 Collabora, Helen Koike <helen.koike@collabora.com>

set -exo pipefail

source ci/gitlab-ci/ci-scripts/ici-functions.sh

ici_prepare_build

pushd build

# compile the entire kernel
make CF=-D__CHECK_ENDIAN__ -C "$ICI_KERNEL_DIR" O=$(pwd) -j$(nproc) 2>&1 | tee output.txt

export INSTALL_PATH="${CI_PROJECT_DIR}/artifacts/"
INSTALL_PATH+="kernel-install-${KCI_KERNEL_ARCH}-${KCI_DEFCONFIG}_config"
mkdir -p "$INSTALL_PATH"

# install the kernel image to artifacts/kernel-install
make -C "$ICI_KERNEL_DIR" O=$(pwd) install INSTALL_PATH="$INSTALL_PATH"

# install kernel modules to artifacts/kernel-install
make -C "$ICI_KERNEL_DIR" O=$(pwd) modules_install INSTALL_MOD_PATH="$INSTALL_PATH"

# export config as artifact
cp .config "${CI_PROJECT_DIR}/artifacts/${KCI_KERNEL_ARCH}-${KCI_DEFCONFIG}_config"

# if the compilation has warnings, exit with the warning code
if grep -iq "warning" output.txt; then
    exit 101
fi

popd
