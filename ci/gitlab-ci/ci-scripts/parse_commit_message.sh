#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024 Collabora, Helen Koike <helen.koike@collabora.com>

set -exo pipefail

# Get the last commit message
commit_message=$(git log -1 --pretty=%B)

pattern='(KCI_[A-Za-z_]+)=("[^"]*"|[^ ]+)'

while read -r line; do
    if [[ $line =~ $pattern ]]; then
        variable_name="${BASH_REMATCH[1]}"
        variable_value="${BASH_REMATCH[2]}"

        # Remove quotes if present
        variable_value="${variable_value%\"}"
        variable_value="${variable_value#\"}"

        # Export the variable
        export "$variable_name=$variable_value"

        echo "Exported $variable_name=$variable_value"
    fi
done <<< "$commit_message"
