#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024 Collabora, Helen Koike <helen.koike@collabora.com>

set -exo pipefail

source ci/gitlab-ci/ci-scripts/ici-functions.sh

ici_get_patch_series_size

# Get the list of modified files in the last $ICI_PATCH_SERIES_SIZE commits
MODIFIED_DOC_FILES=$(git diff HEAD~$ICI_PATCH_SERIES_SIZE --name-only -- Documentation/)

make -j$(nproc) "$ICI_DOC_TYPE" 2>&1 | tee output.txt

mkdir -p "${CI_PROJECT_DIR}/artifacts"
mv Documentation/output "${CI_PROJECT_DIR}/artifacts/Documentation-output"

# Check if any of the MODIFIED_DOC_FILES generated a warning
# NOTE: the alternative solution was to touch the modified files and run make
# again, but too much warnings still appears
for file in $MODIFIED_DOC_FILES; do
    if grep -qi "warning" output.txt && grep -q "$file" output.txt; then
        echo "Warning found in $file"
        exit 101
    fi
done

if [ -n "$ICI_UNABLE_TO_DETECT_PATCH_SERIES_SIZE" ]; then
    # If the patch series size was not detected, exit with a warning
    echo -n "The patch series size was not detected, we probably didn't check the"
    echo " whole series. Exiting with a warning."
    exit 101
fi
