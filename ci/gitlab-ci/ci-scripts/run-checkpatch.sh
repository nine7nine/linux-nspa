#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024 Collabora, Helen Koike <helen.koike@collabora.com>

set -exo pipefail

source ci/gitlab-ci/ci-scripts/ici-functions.sh

ici_get_patch_series_size

# shellcheck disable=SC2086
scripts/checkpatch.pl $KCI_CHECKPATCH_OPTIONS --git HEAD-"$ICI_PATCH_SERIES_SIZE"

if [ -n "$ICI_UNABLE_TO_DETECT_PATCH_SERIES_SIZE" ]; then
    # If the patch series size was not detected, exit with a warning
    echo -n "The patch series size was not detected, we probably didn't check the whole series."
    echo " Exiting with a warning."
    exit 101
fi
