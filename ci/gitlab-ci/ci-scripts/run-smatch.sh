#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024 Collabora, Helen Koike <helen.koike@collabora.com>

set -exo pipefail

source ci/gitlab-ci/ci-scripts/ici-functions.sh

ls -l
pwd

# generate config file
ici_prepare_build

ici_get_patch_series_size

cp build/.config .

# Get a list of modified .c files in the last ICI_PATCH_SERIES_SIZE commits
MODIFIED_C_FILES=$(git diff --name-only HEAD~$ICI_PATCH_SERIES_SIZE HEAD | grep '\.c$' || true)

# Check if any .c files were modified
if [ -z "$MODIFIED_C_FILES" ]; then
    echo "No .c files were modified in the last $ICI_PATCH_SERIES_SIZE commits."
else
    echo "Running kchecker on modified .c files..."
    mkdir -p "$CI_PROJECT_DIR"/artifacts
fi

# Run kchecker on each modified .c file
for file in $MODIFIED_C_FILES; do
    if [ -f "$file" ]; then
        /smatch/smatch_scripts/kchecker "$file" | tee "$CI_PROJECT_DIR"/artifacts/smatch_warns.txt
    else
        echo "File not found: $file"
    fi
done

if [ -n "$ICI_UNABLE_TO_DETECT_PATCH_SERIES_SIZE" ]; then
    # If the patch series size was not detected, exit with a warning
    echo -n "The patch series size was not detected, we probably didn't check the whole series."
    echo " Exiting with a warning."
    exit 101
fi
