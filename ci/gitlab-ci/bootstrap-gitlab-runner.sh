#!/bin/bash
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2024 Collabora, Helen Koike <helen.koike@collabora.com>

set -eo pipefail

# Define variables
CONFIG_VOLUME="/srv/gitlab-runner/config" # Path to your GitLab Runner config

# Check if RUNNER_REGISTRATION_TOKEN is set
if [ -z "${RUNNER_REGISTRATION_TOKEN}" ]; then
    echo "Error: RUNNER_REGISTRATION_TOKEN is not set."
    echo "Please set the RUNNER_REGISTRATION_TOKEN environment variable and try again."
    exit 1
fi

# Check if GITLAB_URL is set
if [ -z "${GITLAB_URL}" ]; then
    GITLAB_URL="https://gitlab.com/"
    echo "Info: GITLAB_URL is not set. Using the default $GITLAB_URL"
    echo "Please set the RUNNER_REGISTRATION_TOKEN environment variable and try again."
fi

# Check if docker-compose is installed
if ! command -v docker-compose &> /dev/null
then
    echo "docker-compose could not be found. Please install it first."
    exit 1
fi

# Start the GitLab Runner using Docker Compose
echo "Starting GitLab Runner..."
docker-compose up -d

# Wait for a few seconds to ensure the service is up
sleep 5

# Register the GitLab Runner
echo "Registering GitLab Runner..."
docker run --rm -v ${CONFIG_VOLUME}:/etc/gitlab-runner gitlab/gitlab-runner register \
  --non-interactive \
  --url ${GITLAB_URL} \
  --token ${RUNNER_REGISTRATION_TOKEN} \
  --executor docker \
  --docker-image "alpine:latest" \
  --description "Docker Runner" \
  --docker-privileged

echo ""
echo "INFO: To configure the number of concurrent jobs, edit the value of"
echo "INFO: concurrent in ${CONFIG_VOLUME}/config.toml, than restart the GitLab"
echo "INFO: Runner using docker-compose restart"
echo ""
echo "GitLab Runner setup complete."
